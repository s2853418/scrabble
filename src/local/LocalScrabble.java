package local;

import scrabble.model.Game;
import scrabble.model.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class LocalScrabble {

    public static void main(String[] args) {
        System.out.println("Enter players names with spaces between them: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String[] inputArray = input.split(" ");
        List<Player> players = new ArrayList<>();
        for(String name:inputArray){
            players.add(new Player(name));
        }
        Game game = new Game(players);
        game.start();

    }
}
