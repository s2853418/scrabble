package exceptions;

public class InvalidMoveException extends Exception{

    public InvalidMoveException() {
        super("Invalid coordinates for desired move");
    }
}
