package exceptions;

public class DuplicateNameException extends Exception{

    public DuplicateNameException() {
        super("Duplicate name trying to join the server");
    }
}
