package exceptions;

public class OutOfTurn extends Exception{

    public OutOfTurn(String name) {
        super("Not "+name+"'s turn");
    }
}
