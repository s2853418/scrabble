package exceptions;

public class UnrecognizedException extends Exception{

    public UnrecognizedException() {
        super("Message cannot be interpreted");
    }
}
