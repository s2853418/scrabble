package server;

import exceptions.DuplicateNameException;
import exceptions.InvalidMoveException;
import scrabble.model.OnlineGame;
import scrabble.model.Player;
import scrabble.services.ProtocolMessages;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

public class ClientHandler implements Runnable{

    private Socket clientSocket;
    private ScrabbleServer server;
    private BufferedWriter output;
    private BufferedReader input;
    private String name;
    private OnlineGame game;

    private boolean winner;
    private boolean hasAborted;

    public ClientHandler(Socket clientSocket, ScrabbleServer server) {
        this.clientSocket = clientSocket;
        this.server = server;
        output = null;
        input = null;
        game = null;
        winner = false;
        hasAborted = false;
    }

    @Override
    public void run() {
        try {
            output = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            try {
                String line;
                while((line = input.readLine())!=null){
                    handleInput(line);
                }
            } catch (SocketException e){
                try {
                    if(server.isActiveGame()){
                        server.doMove(ProtocolMessages.ABORT + ProtocolMessages.SEPARATOR + name);
                        hasAborted = true;
                        server.getClients().remove(this); //remove the current handler from the list of clients(clientHandlers)
                        String msg = ProtocolMessages.ABORT + ProtocolMessages.SEPARATOR + getName();
                        server.getClientsInGame().remove(this);
                        server.sendToClientsInGame(msg);
                        if(server.isGameOver()){
                            server.getGame().printWinner();
                            server.setActiveGame(false);
                            String msgGameOver = ProtocolMessages.GAMEOVER + ProtocolMessages.SEPARATOR + "winnerName";
                            server.sendToClientsInGame(msgGameOver);
                            server.sendToPlayers("A game has finished!");
                            server.getGame().reset();
                            server.setClientsInGame(new ArrayList<>());
                        }
                    }
                    if(server.getQueue().contains(this)){
                        server.getQueue().remove(this);
                    }
                    server.getClients().remove(this);
                    server.getServerView().showMessage("A client has disconnected!");
                    server.setNext_client_no(getServer().getNext_client_no()-1);
                } catch (InvalidMoveException ex) {

                    }
                }

        }
        catch (IOException e) {
            e.printStackTrace();

        }
        finally {
                try {
                    if(output != null){
                        output.close();
                    }
                    if(input != null) {
                        input.close();
                    }
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    private void handleInput(String line) {
        String[] lineArray = line.split(ProtocolMessages.SEPARATOR);
        String command = lineArray[0];
        //System.out.println(line);
        switch(command){
            case ProtocolMessages.HELLO: {
                try {
                    setName(lineArray[1]);
                    server.getClients().add(this);
                } catch (DuplicateNameException e) {
                    String msg = ProtocolMessages.ERROR + ProtocolMessages.SEPARATOR + ProtocolMessages.DUPLICATE_NAME;
                    sendMessage(msg);
                    break;
                }
                sendMessage(ProtocolMessages.HELLO + ProtocolMessages.SEPARATOR + server.helloResponse());

                String msg = ProtocolMessages.WELCOME + ProtocolMessages.SEPARATOR + getName();
                server.sendToAll(msg);

                String firstServerReady = ProtocolMessages.SERVERREADY + ProtocolMessages.SEPARATOR + server.clientReadyResponse();
                sendMessage(firstServerReady);
                break;
            }
            case ProtocolMessages.CLIENTREADY: {
                server.addClientToQueue(this);
                sendMessage(ProtocolMessages.SERVERREADY + ProtocolMessages.SEPARATOR + server.clientReadyResponse());

                if(server.getQueue().size()>1 && !server.isActiveGame()){
                    server.prepareGame();

                    String msg = ProtocolMessages.START + ProtocolMessages.SEPARATOR + server.startResponse();
                    server.sendToClientsInGame(msg);

                    server.doGameSituation();

                    String msg2 = ProtocolMessages.TILES + ProtocolMessages.SEPARATOR + server.getGame().tilesToString();
                    server.getGame().getCurrentPlayer().getHandler().sendMessage(msg2);

                    String msg3 = ProtocolMessages.TURN + ProtocolMessages.SEPARATOR + server.getGame().getCurrentPlayer().getHandler().getName();
                    server.sendToClientsInGame(msg3);

                } else if(server.isActiveGame()){
                    sendMessage("\nThere is a game happening right now.\nWhen it's finished you'll move up the queue");
                }
                break;
            }
            case ProtocolMessages.MOVE: {
                //make move or error code
                try {
                    server.doMove(line);
                    String msg = ProtocolMessages.MOVE + ProtocolMessages.SEPARATOR + name + ProtocolMessages.SEPARATOR + lineArray[2]
                            + ProtocolMessages.SEPARATOR + 99999;
                    server.sendToClientsInGame(msg);
                } catch (InvalidMoveException e) {
                    String errorMsg = ProtocolMessages.ERROR + ProtocolMessages.SEPARATOR + ProtocolMessages.INVALID_MOVE;
                    server.sendToClientsInGame(errorMsg);
                    server.getGame().changeCurrentPlayerIndex();
                    server.getGame().gameSituation();
                }

                //check if game over
                if(server.isGameOver()){
                    server.getGame().printWinner();
                    server.setActiveGame(false);
                    String msgGameOver = ProtocolMessages.GAMEOVER + ProtocolMessages.SEPARATOR + "winnerName";
                    server.sendToClientsInGame(msgGameOver);
                    server.sendToPlayers("A game has finished!");
                    server.getGame().reset();
                    server.setClientsInGame(new ArrayList<>());
                }

                String msg2 = ProtocolMessages.TILES + ProtocolMessages.SEPARATOR + server.getGame().tilesToString();
                server.getGame().getCurrentPlayer().getHandler().sendMessage(msg2);

                String msg3 = ProtocolMessages.TURN + ProtocolMessages.SEPARATOR + server.getGame().getCurrentPlayer().getHandler().getName();
                server.sendToClientsInGame(msg3);
                break;
            }
            case ProtocolMessages.PASS: {
                //swap tiles (or not) and pass the turn
                try {
                    server.doMove(line);
                    String msg = ProtocolMessages.PASS + ProtocolMessages.SEPARATOR + name;
                    server.sendToClientsInGame(msg);
                } catch (InvalidMoveException e) {
                    String errorMsg = ProtocolMessages.ERROR + ProtocolMessages.SEPARATOR + ProtocolMessages.INVALID_MOVE;
                    server.sendToClientsInGame(errorMsg);
                    server.getGame().changeCurrentPlayerIndex();
                    server.getGame().gameSituation();
                }

                String msg2 = ProtocolMessages.TILES + ProtocolMessages.SEPARATOR + server.getGame().tilesToString();
                server.getGame().getCurrentPlayer().getHandler().sendMessage(msg2);

                String msg3 = ProtocolMessages.TURN + ProtocolMessages.SEPARATOR + server.getGame().getCurrentPlayer().getHandler().getName();
                server.sendToClientsInGame(msg3);
                break;
            }
            case ProtocolMessages.ABORT: {
                //disconnect from server
                hasAborted = true;
                try {
                    server.doMove(line);
                } catch (InvalidMoveException e) {
                    String errorMsg = ProtocolMessages.ERROR + ProtocolMessages.SEPARATOR + ProtocolMessages.INVALID_MOVE;
                    sendMessage(errorMsg);
                }
                server.getClients().remove(this); //remove the current handler from the list of clients(clientHandlers)
                String msg = ProtocolMessages.ABORT + ProtocolMessages.SEPARATOR + getName();
                server.sendToClientsInGame(msg);
                if(server.isGameOver()){
                    server.getGame().printWinner();
                    server.setActiveGame(false);
                    String msgGameOver = ProtocolMessages.GAMEOVER + ProtocolMessages.SEPARATOR + "winnerName";
                    server.sendToClientsInGame(msgGameOver);
                    server.sendToPlayers("A game has finished!");
                    server.getGame().reset();
                    server.setClientsInGame(new ArrayList<>());
                }
                server.getClients().remove(this);
                server.getServerView().showMessage("A client has disconnected!");
                server.setNext_client_no(getServer().getNext_client_no()-1);
                break;
            }
            case ProtocolMessages.NEW_GAME: {
                String msg = ProtocolMessages.SERVERREADY + ProtocolMessages.SEPARATOR + server.clientReadyResponse();
                sendMessage(msg);
                break;
            }
            default: {
                System.out.println("Invalid input");
                break;
            }

        }

    }

    public void sendMessage(String msg){
        if(output!=null){
            try {
                output.write(msg);
                output.newLine();
                output.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setName(String name) throws DuplicateNameException {
        boolean duplicate = false;
        if(server.getClients().size()>0){
            for(ClientHandler handler : server.getClients()) {
                if(handler.getName().equals(name)){
                    duplicate = true;
                    break;
                }
            }
            if(duplicate){
                throw new DuplicateNameException();
            } else {
                this.name = name;
            }
        } else {
            this.name = name;
        }


    }

    public String getName(){
        return name;
    }
    public ScrabbleServer getServer() {
        return server;
    }

    public BufferedWriter getOutput() {
        return output;
    }
}
