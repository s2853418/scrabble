package server.view;

import java.io.PrintWriter;
import java.util.Scanner;

public class ServerView {

    private PrintWriter console;
    private Scanner scanner;

    public ServerView() {
        console = new PrintWriter(System.out,true);
        scanner = new Scanner(System.in);
    }

    public void showMessage(String msg){
        console.println(msg);
    }

}
