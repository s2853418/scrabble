package server;

import exceptions.InvalidMoveException;
import scrabble.model.Game;
import scrabble.model.OnlineGame;
import scrabble.model.Player;
import scrabble.services.ProtocolMessages;
import server.view.ServerView;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Scanner;

public class ScrabbleServer implements Runnable{

    //private static final int SERVER_PORT = 9090;

    private int port;
    private ServerSocket server;
    private ServerView serverView;
    private List<ClientHandler> clients;
    private List<ClientHandler> queue;
    private List<ClientHandler> clientsInGame;
    private int next_client_no;

    private boolean activeGame;
    private boolean gameOver;
    private OnlineGame game;

    public ScrabbleServer() {
        clients = new ArrayList<>();
        serverView = new ServerView();
        next_client_no = 1;
        port = 0;
        queue = new ArrayList<>();
        activeGame = false;
        gameOver = false;
        clientsInGame = new ArrayList<>();
    }

    public void setPort(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("What port should the server use: ");
        String portString = scanner.nextLine();
        this.port = Integer.parseInt(portString);
    }

    @Override
    public void run() {

        try {
            setup();

            while(true){
                Socket socket = server.accept();
                String name = "Client "
                        + String.format("%02d", next_client_no++);
                serverView.showMessage("New client [" + name + "] connected!");
                ClientHandler handler =
                        new ClientHandler(socket, this);
                new Thread(handler).start();
                //addClientToQueue(handler);
            }
        } catch (IOException e) {

        }

    }

    public void prepareGame() {
        String name1 = queue.get(0).getName();
        String name2 = queue.get(1).getName();
        Player player1 = new Player(name1);
        Player player2 = new Player(name2);
        player1.setHandler(queue.get(0));
        player2.setHandler(queue.get(1));

        ArrayList<Player> players = new ArrayList<>();
        players.add(player1);
        players.add(player2);

        game = new OnlineGame(players);
        game.reset();
        activeGame = true;

        clientsInGame.add(queue.get(0));
        clientsInGame.add(queue.get(1));

        queue.remove(0);
        queue.remove(0);
//        String msg = "";
//        for(ClientHandler handler : queue) {
//            msg = msg + handler.getName() + " ";
//        }
//        System.out.println("The queue consists of: " + msg);
    }

    public void doGameSituation(){
        game.gameSituation();
    }

    public void doMove(String input) throws InvalidMoveException {
        if(!game.gameOver()){
            if(!game.makeMove(input)){
                throw new InvalidMoveException();
            }
            if(!game.gameOver()){
                game.changeCurrentPlayerIndex();
                game.gameSituation();
            } else {
                game.deductUnplayedLetters();
                gameOver = true;
                //game.printWinner();
            }
        } else {
            game.deductUnplayedLetters();
            gameOver = true;
            //game.printWinner();
        }
    }

    public void addClientToQueue(ClientHandler handler) {
        queue.add(handler);
    }

    private void setup() {

        server = null;
        while (server == null) {
            try {
                serverView.showMessage("Attempting to open a socket at 127.0.0.1 "
                        + "on port " + port + "...");
                server = new ServerSocket(port, 0,
                        InetAddress.getByName("127.0.0.1"));
                serverView.showMessage("Server started at port " + port);
            } catch (IOException e) {
                serverView.showMessage("ERROR: could not create a socket on "
                        + "127.0.0.1" + " and port " + port + ".");
            }
        }

    }

    public List<ClientHandler> getQueue() {
        return queue;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public boolean isActiveGame() {
        return activeGame;
    }

    public List<ClientHandler> getClients() {
        return clients;
    }

    public String helloResponse(){
        String names = "";
        for(ClientHandler handler : clients){
            names = names + handler.getName() + ProtocolMessages.AS;
        }
        return names;
    }

    public String clientReadyResponse() {
        String names = "";
        for(ClientHandler handler : queue){
            names = names + handler.getName() + ProtocolMessages.AS;
        }
        return names;
    }

    public String startResponse(){
        String names = "";
        for(ClientHandler handler : clientsInGame){
            names = names + handler.getName() + ProtocolMessages.AS;
        }
        return names;
    }

    public void sendToAll(String msg){
        for(ClientHandler handler : clients){
            handler.sendMessage(msg);
        }

    }

    public void sendToPlayers(String msg){
        for(ClientHandler handler : queue){
            handler.sendMessage(msg);
        }
    }

    public void sendToClientsInGame(String msg) {
        for(ClientHandler handler : clientsInGame){
            handler.sendMessage(msg);
        }
    }

    public static void main(String[] args) {
        ScrabbleServer scrabbleServer = new ScrabbleServer();
        scrabbleServer.setPort();
        System.out.println("Starting Scrabble server...");
        new Thread(scrabbleServer).start();
    }

    public OnlineGame getGame() {
        return game;
    }

    public List<ClientHandler> getClientsInGame() {
        return clientsInGame;
    }

    public void setActiveGame(boolean activeGame) {
        this.activeGame = activeGame;
    }

    public void setClientsInGame(List<ClientHandler> clientsInGame) {
        this.clientsInGame = clientsInGame;
    }

    public int getNext_client_no() {
        return next_client_no;
    }

    public void setNext_client_no(int next_client_no) {
        this.next_client_no = next_client_no;
    }

    public ServerView getServerView() {
        return serverView;
    }
}


