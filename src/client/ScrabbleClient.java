package client;

import client.view.ScrabbleClientView;
import exceptions.UnrecognizedException;
import scrabble.services.ProtocolMessages;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class ScrabbleClient implements Runnable {

//    private static final String SERVER_IP = "127.0.0.1";
//    private static final int SERVER_PORT = 9090;

    private String name;
    private Socket socket;
    private BufferedReader input;
    private BufferedWriter output;
    private ScrabbleClientView scrabbleClientView;

    private boolean ready;


    public ScrabbleClient() {
        this.scrabbleClientView = new ScrabbleClientView(this);
        ready = false;
    }


    public void createConnection(){
        while (socket == null) {
            String host = "127.0.0.1";
            int port = Integer.parseInt(scrabbleClientView.getInput("What port to connect to?"));


            // try to open a Socket to the server
            try {
                InetAddress addr = InetAddress.getByName(host);
                scrabbleClientView.showMessage("Attempting to connect to " + addr + ":" + port + "...\n");
                socket = new Socket(addr, port);
                input = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));
                output = new BufferedWriter(new OutputStreamWriter(
                        socket.getOutputStream()));
                scrabbleClientView.showMessage("Connected to " + addr + ":" + port + "\n");
            } catch (IOException e) {
                scrabbleClientView.showMessage("ERROR: could not create a socket on " + host + " and port " + port + ".");
            }
        }
    }

    public void run() {
        createConnection();
        try {
            //System.out.println(client.getSocket().isClosed());

            handleHello(getScrabbleClientView().getInput("What should be your username? (cannot be the same as clients already in the server)"));
            while(!getSocket().isClosed()) {
                String line = getInputBR().readLine();
                handleServerMessage(line);
                //client.getScrabbleClientView().showMessage(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleServerMessage(String line){
//        System.out.println("the line is - " + line);
        String lineArray[] = null;
        String command = "";
            if(line.contains(ProtocolMessages.SEPARATOR)){
                lineArray = line.split(ProtocolMessages.SEPARATOR);
                command = lineArray[0];
            }

            switch (command){
                case ProtocolMessages.ERROR: {
                    handleError(lineArray[1]);
                    break;
                }
                case ProtocolMessages.HELLO: {
                    getScrabbleClientView().showMessage("The client(s): " + lineArray[1] + "are connected to the server");
                    break;
                }
                case ProtocolMessages.WELCOME: {
                    getScrabbleClientView().showMessage("Client " + lineArray[1] + " just connected");
                    break;
                }
                case ProtocolMessages.SERVERREADY:{
//                    try {
//                        TimeUnit.SECONDS.sleep(10);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                    if(!ready){
                        String input = getScrabbleClientView().getInput("Are you ready to start the game? (yes/no)");
                        while(!input.equals("yes")){
                            input = getScrabbleClientView().getInput("Are you ready to start the game? (yes/no)");
                        }
                        ready = true;
                        String msg = ProtocolMessages.CLIENTREADY + ProtocolMessages.SEPARATOR + getName();
                        sendMessage(msg);
                    }else {
                        String msg = "The client(s) ready are: " + lineArray[1];
                        scrabbleClientView.showMessage(msg);
                    }
                    break;
                }
                case ProtocolMessages.START: {
                    scrabbleClientView.showMessage("\nGame is starting...");
                    scrabbleClientView.showMessage("The clients to play in the game are: " + lineArray[1]);
                    break;
                }
                case ProtocolMessages.TILES: {
                    String msg = "\nYour tiles are: " + lineArray[1];
                    scrabbleClientView.showMessage(msg);
                    break;
                }
                case ProtocolMessages.TURN: {
                    if(lineArray[1].equals(name)){
                        scrabbleClientView.showMessage("It is your turn!");
//                        while(true){
//                            String input = scrabbleClientView.getInput(scrabbleClientView.menu());
//                            try {
//                                scrabbleClientView.handleUserInput(input);
//                                break;
//                            } catch (UnrecognizedException e) {
//                                scrabbleClientView.showMessage("\nInvalid input. Write a valid command.\n");
//                            }
//                        }
//                        String[] inputArray = input.split(":");
//                        String commandFromPlayer = inputArray[0];
//                        String argument = inputArray[1
                        String input = scrabbleClientView.getInput(scrabbleClientView.menu());
                        try {
                            scrabbleClientView.handleUserInput(input);
                        } catch (UnrecognizedException e) {
                            scrabbleClientView.showMessage("Invalid input");
                        }
                    } else {
                        scrabbleClientView.showMessage("\nIt is NOT your turn!\nWait for the other player's turn to finish...");
                    }
                    break;
                }
                case ProtocolMessages.MOVE: {
                    scrabbleClientView.showMessage("\nA valid word has been placed by: " + lineArray[1]);
                    break;
                }
                case ProtocolMessages.PASS: {
                    String msg = "\n"+lineArray[1] + " just passed his/her turn";
                    scrabbleClientView.showMessage(msg);
                    break;
                }
                case ProtocolMessages.ABORT: {
                    scrabbleClientView.showMessage(lineArray[1]+" has left the game");
                    break;
                }
                case ProtocolMessages.GAMEOVER: {
                    //winner printed by printWinner, ask for a new game
                    String play = scrabbleClientView.getInput("Do you want to play another game (you'll be returned to the queue)? (yes/no)");
                    if(play.equals("yes")) {
                        ready = false;
                        String msg = ProtocolMessages.NEW_GAME;
                        sendMessage(msg);
                    } else {
                        closeConnection();
                    }
                    break;
                }
                default: {
                    scrabbleClientView.showMessage(line);
                }
            }
        }

    public void resetConnection(){
        socket = null;
        input = null;
        output = null;
    }

    public void sendMessage(String msg){
        if(output!=null){
            try {
                output.write(msg);
                output.newLine();
                output.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void handleError(String errorName){

        if(errorName.equals(ProtocolMessages.DUPLICATE_NAME)){
            handleHello(getScrabbleClientView().getInput("\nThere is already a player with this name in the server.\nPlease, choose a different username:"));
        }

        if(errorName.equals(ProtocolMessages.INVALID_MOVE)){
            //String name =
            scrabbleClientView.showMessage("\nAn invalid move has been made. The turn of the player is skipped.\n");
        }

    }

    public boolean serverMessage() {
//        if(input != null){
//            String answer = null;
//            try {
//                answer = input.readLine();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if(answer == null){
//                return true;
//            } else {
//                return false;
//            }
//        } else {
//            return false;
//        }
        try {
            if(input.ready()){
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String readLineFromServer(){
        if(input != null) {
            try {
                String answer = input.readLine();
                if(answer == null){
                    //throw some exception
                }
                return answer;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            //throw some exception
        }
        return  null;
    }

//    public void printMultipleLinesFromServer(){
//        if(input!=null){
//            try {
//                String line;
//////                System.out.println(input.ready());
//////                System.out.println(input.readLine());
//////                System.out.println(input.readLine());
//                while ((line = input.readLine()) != null) {
//                    if(line.equals(ProtocolMessages.EOT)){
//                        break;
//                    }
//                    scrabbleClientView.showMessage(line);
//                }
//                //input.lines().forEach(System.out::println);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    public String readMultipleLinesFromServer() {
        if (input != null) {
            try {
                // Read and return answer from Server
                //StringBuilder sb = new StringBuilder();
                List<String> lines = new ArrayList<>();
                for (String line = input.readLine(); line != null; line = input.readLine()) {
                    //sb.append(line + System.lineSeparator());
                    System.out.println(line);
                    lines.add(line);
                }
                input.close();
                String result = "";
                for(String line : lines){
                    result = result + line + "\n";
                }
                return result;
            } catch (IOException e) {
                //throw some exception
            }
        } else {
            //throw some exception
        }
        return null;
    }

    public void closeConnection(){
        try {
            input.close();
            output.close();
            socket.close();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleHello(String name) {
        setName(name);
        sendMessage(ProtocolMessages.HELLO + ProtocolMessages.SEPARATOR + name + ProtocolMessages.SEPARATOR + "no timer");
    }

    public void handleReady() {
        String msg = ProtocolMessages.CLIENTREADY + ProtocolMessages.SEPARATOR + name;
        sendMessage(msg);
        //printMultipleLinesFromServer();
//        String answer = readMultipleLinesFromServer();
//        scrabbleClientView.showMessage(answer); //show ServerReady with ready players and START if two players ready
    }

    public void handleAbort(){
        String msg = ProtocolMessages.ABORT + ProtocolMessages.SEPARATOR + name;
        sendMessage(msg);
    }

    public void handleMove(String coordinates) {
        String msg = ProtocolMessages.MOVE + ProtocolMessages.SEPARATOR + name + ProtocolMessages.SEPARATOR + coordinates;
        sendMessage(msg);
        //String answer = readLineFromServer();
        //scrabbleClientView.showMessage(answer); //notify all about a move
    }

    public void handlePass(String letters) {
        String msg = ProtocolMessages.PASS + ProtocolMessages.SEPARATOR + letters;
        sendMessage(msg);
        //String answer = readMultipleLinesFromServer();
        //scrabbleClientView.showMessage(answer);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Socket getSocket() {
        return socket;
    }

    public BufferedReader getInputBR() {
        return input;
    }

    public ScrabbleClientView getScrabbleClientView() {
        return scrabbleClientView;
    }

    public void setScrabbleClientView(ScrabbleClientView scrabbleClientView) {
        this.scrabbleClientView = scrabbleClientView;
    }

    public static void main(String[] args) {
        ScrabbleClient client = new ScrabbleClient();
        new Thread(client).start();
    }


}

