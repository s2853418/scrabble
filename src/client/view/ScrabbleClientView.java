package client.view;

import client.ScrabbleClient;
import exceptions.UnrecognizedException;
import scrabble.services.ProtocolMessages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class ScrabbleClientView{

    private ScrabbleClient client;
    private Scanner scanner;

    public ScrabbleClientView(ScrabbleClient client){
        this.client = client;
        this.client.setScrabbleClientView(this);
        scanner = new Scanner(System.in);
    }

    public String menu(){
        //String line1 = "General format of commands:  [COMMAND][SEPERATOR][ARGUMENT1][SEPERATOR][ARGUMENT2]... where SEPARATOR is " + ProtocolMessages.SEPARATOR + "\n";
        String line2 = "\nTo place a word write: " + ProtocolMessages.MOVE + ":[LETTER,INDEX] [LETTER,INDEX] .. example "+ProtocolMessages.MOVE+":D1 O2 -G3\n* the blank tile is represented by -, to assign it a letter, write the letter after the - *\n";
        String line3 = "To pass a turn: " + ProtocolMessages.PASS + ":[TILES TO BE SWAPPED] (can be left empty) example "+ProtocolMessages.PASS+":A B C or "+ProtocolMessages.PASS+":\n";
        String line4 = "To leave the game: A"+"\n\nWhat's your move?";
        String menu = line2 + line3 + line4;
        return menu;
    }

    public void handleUserInput(String in) throws UnrecognizedException {
        String[] inArray = in.split(":");
        String command = inArray[0];
        String argument = "";
        if(inArray.length>1){
            argument = inArray[1];
        }
        switch(command) {
            case ProtocolMessages.MOVE: {
                client.handleMove(argument);
                break;
            }
            case ProtocolMessages.PASS: {
                client.handlePass(argument);
                break;
            }
            case ProtocolMessages.ABORT: {
                client.handleAbort();
                client.closeConnection();
                break;
            }
            default: {
                throw new UnrecognizedException();
            }
        }
    }

    public void showMessage(String msg){
        System.out.println(msg);
    }

    public String getInput(String msg) {
        showMessage(msg);
        String in = scanner.nextLine();
        return in;
    }

//    public String getInput(String msg) {
//        showMessage(msg);
//        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//        String input=null;
////        do {
////            //System.out.println("Please type something: ");
////            try {
////                // wait until we have data to complete a readLine()
////                while (!br.ready()  /*  ADD SHUTDOWN CHECK HERE */) {
////                    Thread.sleep(20000);
////                }
////                input = br.readLine();
////                if(input.isEmpty()){
////                    System.out.println("hui");
////                }
////            } catch (InterruptedException e) {
////                System.out.println("ConsoleInputReadTask() cancelled");
////                return null;
////            } catch (IOException e) {
////                e.printStackTrace();
////            }
////        } while ("".equals(input));
////        Timer timer = new Timer();
////        timer.schedule(new TimerTask() {
////            String input = null;
////            public void run() {
////                try {
////                    input = br.readLine();
////                } catch (IOException e) {
////                    e.printStackTrace();
////                }
////            }
////        }, 500 , 1000);
//        String in = scanner.nextLine();
//        //String in = scanner.nextLine();
//        return in;
//    }

}
