package scrabble.test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import scrabble.model.Board;
import scrabble.model.OnlineGame;
import scrabble.model.Player;
import scrabble.model.Tile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static scrabble.model.Board.allTiles;

class OnlineGameTest {

    Player player1 = new Player("Player 1");
    Player player2 = new Player("Player 2");
    private final ArrayList<Player> players = new ArrayList<>();
    OnlineGame game;

    @BeforeEach
    void setUp() {
        players.add(player1);
        players.add(player2);
        game = new OnlineGame(players);
        game.reset();
    }

    @Test
    void testReset() {
        game.reset();
        int all = 0;
        for(int quantity : allTiles().values()){
            all = all + quantity;
        }
        assertEquals(100,all);
        assertEquals(86,game.getAvailableTiles().getAvailableTiles().size());
        assertEquals(14,game.getDecks().get(player1).size()+game.getDecks().get(player2).size());
    }

    @Test
    void testDeductUnplayedLetters() {
        game.getDecks().get(player1).clear();
        game.getDecks().get(player2).clear();
        Map<Player, List<Tile>> test = new HashMap<>();
        ArrayList<Tile> testDeck = new ArrayList<>();
        testDeck.add(new Tile('A',1));
        testDeck.add(new Tile('A',1));
        test.put(player1,testDeck);
        test.put(player2,testDeck);
        game.setDecks(test);
        game.deductUnplayedLetters();
        game.getAvailableTiles().getAvailableTiles().clear();
        assertEquals(-2,game.getScores().get(player1));
    }

    @Test
    void testGameOver() {
        game.getPlayers().remove(0);
        assertTrue(game.gameOver()); //check less than 2 players
        game.getPlayers().add(player1);
        game.makeMove("PASS:;:");
        game.changeCurrentPlayerIndex();
        game.makeMove("PASS:;:");
        game.getAvailableTiles().getAvailableTiles().clear();
        assertTrue(game.gameOver()); //check
    }

    @Test
    void testPossibleMove() {
        game.makeMove("PASS:;:");
        game.makeMove("PASS:;:");
        game.getAvailableTiles().getAvailableTiles().clear();
        assertFalse(game.possibleMove());
    }

    @Test
    void testTilesToString() {
        game.getDecks().get(player1).clear();
        game.getDecks().get(player1).clear();
        game.getDecks().get(player2).clear();
        Map<Player, List<Tile>> test = new HashMap<>();
        ArrayList<Tile> testDeck = new ArrayList<>();
        testDeck.add(new Tile('A',1));
        testDeck.add(new Tile('A',1));
        test.put(player1,testDeck);
        test.put(player2,testDeck);
        game.setDecks(test);
        assertEquals("| A | | A | ",game.tilesToString());
    }

    @Test
    void testGetIndexFromLetterIndex() {
        String letterIndex1 = "A1";
        String letterIndex2 = "A23";
        String letterIndex3 = "A121";
        assertEquals(1,game.getIndexFromLetterIndex(letterIndex1));
        assertEquals(23,game.getIndexFromLetterIndex(letterIndex2));
        assertEquals(121,game.getIndexFromLetterIndex(letterIndex3));
    }

    @Test
    void makeMove() {
        //place a word
        game.getDecks().get(player1).clear();
        game.getDecks().get(player1).clear();
        game.getDecks().get(player2).clear();
        Map<Player, List<Tile>> test = new HashMap<>();
        ArrayList<Tile> testDeck = new ArrayList<>();
        testDeck.add(new Tile('A',1));
        testDeck.add(new Tile('B',1));
        testDeck.add(new Tile('S',1));
        test.put(player1,testDeck);
        test.put(player2,testDeck);
        game.setDecks(test);
        game.makeMove("M:;:sth:;:A112 B113 S114");
        assertEquals(game.getBoard().getSquare(112).getTile().getLetter(),'A');
        assertEquals(game.getBoard().getSquare(113).getTile().getLetter(),'B');
        assertEquals(game.getBoard().getSquare(114).getTile().getLetter(),'S');
        //pass
        game.makeMove("PASS:;:");
        assertEquals(1,game.getNumberOfPasses());
        //abort
        game.makeMove("A:;:");
        assertTrue(game.gameOver());
    }

    @Test
    void testContainsBlankTile() {
        String coordinates1 = "A1 -B2 G3";
        String coordinates2 = "A1 B2 G3";
        assertTrue(game.containsBlankTile(coordinates1));
        assertFalse(game.containsBlankTile(coordinates2));
    }

    @Test
    void testChooseLetter() {
        String coordinates = "A1 -B2 G3";
        assertEquals(game.chooseLetter(coordinates),"A1 B2 G3 ");
    }

    @Test
    void testValidateMove() {
        game.getDecks().get(player1).clear();
        game.getDecks().get(player1).clear();
        game.getDecks().get(player2).clear();
        Map<Player, List<Tile>> test = new HashMap<>();
        ArrayList<Tile> testDeck = new ArrayList<>();
        testDeck.add(new Tile('A',1));
        testDeck.add(new Tile('B',1));
        testDeck.add(new Tile('S',1));
        test.put(player1,testDeck);
        test.put(player2,testDeck);
        game.setDecks(test);
        game.makeMove("M:;:sth:;:A112 B113 S114");
        assertEquals(game.getBoard().getSquare(112).getTile().getLetter(),'A');
        assertEquals(game.getBoard().getSquare(113).getTile().getLetter(),'B');
        assertEquals(game.getBoard().getSquare(114).getTile().getLetter(),'S');
    }

    @Test
    void testValidFirstTurn() {
        String coordinates1 = "A1 B2 C3";
        String coordinates2 = "A112 B2 C3";
        assertFalse(game.validFirstTurn(coordinates1));
        assertTrue(game.validFirstTurn(coordinates2));
    }

    @Test
    void testletterBefore() {
        game.getBoard().placeTile(112,new Tile('A',1));
        assertTrue(game.letterBefore(113));
    }

    @Test
    void testLetterAfter() {
        game.getBoard().placeTile(112,new Tile('A',1));
        assertTrue(game.letterAfter(111));
    }

    @Test
    void testLetterAbove() {
        game.getBoard().placeTile(112,new Tile('A',1));
        assertTrue(game.letterAbove(127));
    }

    @Test
    void testLetterBelow() {
        game.getBoard().placeTile(112,new Tile('A',1));
        assertEquals(game.wordBelow(97),"A");
    }

    @Test
    void testWordBelow() {
        game.getBoard().placeTile(112,new Tile('A',1));
        assertTrue(game.letterBelow(97));
    }

    @Test
    void testWordBefore() {
        game.getBoard().placeTile(112,new Tile('A',1));
        assertEquals(game.wordBefore(113),"A");
    }

    @Test
    void testWordAfter() {
        game.getBoard().placeTile(112,new Tile('A',1));
        assertEquals(game.wordAfter(111),"A");
    }

    @Test
    void testWordAbove() {
        game.getBoard().placeTile(112,new Tile('A',1));
        assertEquals(game.wordAbove(127),"A");
    }

    @Test
    void testAdjacentHorizontal() {
        game.getDecks().get(player1).clear();
        game.getDecks().get(player1).clear();
        game.getDecks().get(player2).clear();
        Map<Player, List<Tile>> test = new HashMap<>();
        ArrayList<Tile> testDeck = new ArrayList<>();
        testDeck.add(new Tile('A',1));
        testDeck.add(new Tile('B',1));
        testDeck.add(new Tile('S',1));
        test.put(player1,testDeck);
        test.put(player2,testDeck);
        game.setDecks(test);
        game.makeMove("M:;:sth:;:A112 B113 S114");
        game.getBoard().placeTile(97,new Tile('B',1));
        game.getBoard().placeTile(127,new Tile('T',1));
        ArrayList<String> testStr= new ArrayList<>();
        testStr.add("BAT");
        assertEquals(testStr,game.adjacentHorizontal("A112 B113 S114"));
    }

    @Test
    void testAdjacentVertical() {
        game.getDecks().get(player1).clear();
        game.getDecks().get(player2).clear();
        Map<Player, List<Tile>> test = new HashMap<>();
        ArrayList<Tile> testDeck = new ArrayList<>();
        testDeck.add(new Tile('A',1));
        testDeck.add(new Tile('B',1));
        testDeck.add(new Tile('S',1));
        test.put(player1,testDeck);
        test.put(player2,testDeck);
        game.setDecks(test);
        game.makeMove("M:;:sth:;:B97 A112 T127");
        game.getBoard().placeTile(113,new Tile('B',1));
        game.getBoard().placeTile(114,new Tile('S',1));
        ArrayList<String> testStr = new ArrayList<>();
        testStr.add("ABS");
        assertEquals(testStr,game.adjacentVertical("B97 A112 T127"));
    }

    @Test
    void testCheckTilesExistInDeck() {
        game.getDecks().get(player1).clear();
        game.getDecks().get(player1).clear();
        game.getDecks().get(player2).clear();
        Map<Player, List<Tile>> test = new HashMap<>();
        ArrayList<Tile> testDeck = new ArrayList<>();
        testDeck.add(new Tile('A',1));
        testDeck.add(new Tile('B',1));
        testDeck.add(new Tile('S',1));
        test.put(player1,testDeck);
        test.put(player2,testDeck);
        game.setDecks(test);
        assertTrue(game.checkTilesExistInDeck("A112 B113 S114"));
    }

    @Test
    void testIsValidWord() {
        assertTrue(game.isValidWord("unit"));
        assertTrue(game.isValidWord("tests"));
        assertTrue(game.isValidWord("are"));
        assertFalse(game.isValidWord("hardd"));
    }

    @Test
    void testCalculateWord() {
        assertEquals(game.calculateWord("MAN"),5);
        assertEquals(game.calculateWord("M!N"),4);
        assertEquals(game.calculateWord("WAR"),6);
    }

    @Test
    void testCalculateCombinedMainWord() {
        Board board = new Board();
        board.placeTile(113,new Tile('E',1));
        board.placeTile(114,new Tile('N',1));
        board.placeTile(112,new Tile('P',1));
        board.placeTile(115,new Tile('S',1));
        game.setBoard(board);
        game.setHorizontal(true);
        assertEquals(2,game.calculateCombinedMainWord(112,115,"P112 S115",false,"P112 S115"));
    }

    @Test
    void testCalculateScore() {
        game.getDecks().get(player1).clear();
        game.getDecks().get(player1).clear();
        game.getDecks().get(player2).clear();
        Map<Player, List<Tile>> test = new HashMap<>();
        ArrayList<Tile> testDeck = new ArrayList<>();
        testDeck.add(new Tile('A',1));
        testDeck.add(new Tile('B',1));
        testDeck.add(new Tile('S',1));
        test.put(player1,testDeck);
        test.put(player2,testDeck);
        game.setDecks(test);
        game.makeMove("M:;:sth:;:A112 B113 S114");
        assertEquals(6,game.calculateScore("A112 B113 S114",false,"A112 B113 S114"));
        game.setBoard(new Board());
        game.setNumberOfValidMoves(0);
        testDeck.add(new Tile('A',1));
        testDeck.add(new Tile('B',1));
        testDeck.add(new Tile('-',1));
        game.makeMove("M:;:sth:;:A112 B113 -S114");
        assertEquals(4,game.calculateScore("A112 B113 -S114",true,"A112 B113 S114"));
    }

    @Test
    void testCheckBingo() {
        assertTrue(game.checkBingo("A1 B2 C3 D4 F5 G6 Y7"));
    }

}