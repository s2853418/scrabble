package scrabble.model;

import static scrabble.model.Board.SIZE;

public class Square {

    private int x;
    private int y;
    private Square.Type type;
    private Tile tile;

    public Square(int y, int x, Type type) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.setTile(null);
    }

    public Type getType() {
        return type;
    }

    public Tile getTile() {
        return tile;
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    //0 to 224
    public int getIndex(){
        int index = SIZE*y + x;
        return index;
    }

    public static int getRowFromIndex(int index){
        return index/SIZE;
    }

    public static int getColFromIndex(int index){
        return index%15;
    }

    public enum Type {
        NORMAL,
        CENTER,
        DOUBLE_LETTER,
        DOUBLE_WORD,
        TRIPLE_LETTER,
        TRIPLE_WORD
    }
}
