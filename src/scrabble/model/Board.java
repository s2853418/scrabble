package scrabble.model;

import scrabble.services.BoardGenerator;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Board {

    public static final int SIZE = 15;
    private Square[][] squares;
    private BoardGenerator boardGenerator;

    public Board() {
        squares = new Square[SIZE][SIZE];
        boardGenerator = new BoardGenerator();
        setSquares(boardGenerator.getEmptyBoard());
    }

    public void setSquares(Square[][] squares) {
        this.squares = squares;
    }

    public Square[][] getSquares() {
        return squares;
    }

    //place tile on board with x,y coordinates
    public Square placeTile(int x, int y, Tile tile) {
            squares[y][x].setTile(tile);
            return squares[y][x];
    }

    //place tile with index of square from 0 to 224
    public Square placeTile(int index, Tile tile) {
        getSquare(index).setTile(tile);
        return getSquare(index);
    }

    public Board copy(){
        Board newBoard = new Board();
        Square[][] newSquares = new Square[SIZE][SIZE];

        for(int row=0;row<SIZE;row++){
            for (int col = 0; col<SIZE;col++){
                Square squaretemp = new Square(this.getSquare(row,col).getY(),this.getSquare(row,col).getX(),this.getSquare(row,col).getType());
                squaretemp.setTile(this.getSquare(row,col).getTile());
                newSquares[row][col] = squaretemp;
            }
        }
        newBoard.setSquares(newSquares);
        return newBoard;
    }

    //get the index of a square
    public int getIndex(int row, int col){
        int index = SIZE*row + col;
        return index;
    }

    public Square getSquare(int row, int col){
        return squares[row][col];
    }

    public Square getSquare(int index){
        int row = index/SIZE;
        int col = index%SIZE;
        return getSquare(row,col);
    }
    //return Map with Tiles(letter and points for letter) as key and quantity as value
    public static Map<Tile,Integer> allTiles(){
        BufferedReader bufferedReader;
        Map <Tile,Integer> result = new HashMap<>();
        try {
            bufferedReader = new BufferedReader(new FileReader("src\\tilesToGenerate.txt"));
            String line = null;
            while((line = bufferedReader.readLine()) != null){
                String[] lineArray = line.split(":");
                char letter = lineArray[0].charAt(0);
                int points = Integer.parseInt(lineArray[1]);
                int quantity = Integer.parseInt(lineArray[2]);
                result.put(new Tile(letter,points),quantity);
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

}
