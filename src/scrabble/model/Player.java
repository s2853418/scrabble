package scrabble.model;

import server.ClientHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Player {

    private String name;
    private int score;
    private List<Tile> deck;
    private ClientHandler handler;

    public Player(String name) {
        this.name = name;
        this.score = 0;
        this.deck = new ArrayList<Tile>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void increaseScore(int score){
        setScore(this.score+score);
    }

    public List<Tile> getDeck() {
        return deck;
    }

    public void setDeck(List<Tile> deck) {
        this.deck = deck;
    }

    public String tryMove(){
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        return input;
    }

    public ClientHandler getHandler() {
        return handler;
    }

    public void setHandler(ClientHandler handler) {
        this.handler = handler;
    }
}
