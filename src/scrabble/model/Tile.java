package scrabble.model;

public class Tile {

    private char letter;
    private int points;



    public Tile(char letter, int points) {
        this.letter = letter;
        this.points = points;
    }

    public char getLetter() {
        return letter;
    }

    public int getPoints() {
        return points;
    }

    @Override
    public String toString(){
//        String newLine = System.getProperty("line.separator");
//        return "┎───┒ "
//                .concat(newLine)
//                .concat("| "+String.valueOf(this.letter) + " | ")
//                .concat(newLine)
//                .concat("┖───┚ ");
        return "| "+ this.letter + " |";
    }

    public static void main(String[] args) {
        Tile tile= new Tile('A',12);
        Tile tile2= new Tile('B',12);
        System.out.println(tile.toString());
    }
}
