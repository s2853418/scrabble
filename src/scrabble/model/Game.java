package scrabble.model;

import scrabble.services.ProtocolMessages;
import scrabble.services.TilesNotInGame;
import scrabble.view.utils.TextBoardRepresentation;
import wordChecker.InMemoryScrabbleWordChecker;
import wordChecker.ScrabbleWordChecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Game {

    public static final int BINGO = 50;

    private Board board;
    private TextBoardRepresentation boardRepresentation;
    private List<Player> players;
    private TilesNotInGame availableTiles;
    private Map<Player,List<Tile>> decks = new HashMap<>();
    private Map<Player, Integer> scores = new HashMap<>();
    private int currentPlayerIndex;
    private boolean horizontal;
    private int numberOfValidMoves;
    private int numberOfPasses;

    private Map<String,List<Square>> wordsOnBoard;
    private List <Square> mainWordSquares;

    private ScrabbleWordChecker scrabbleWordChecker = new InMemoryScrabbleWordChecker();

    /**
     * Constructor of Game class
     * @param players to play the game
     */
    public Game(List<Player> players) {
        Collections.shuffle(players);
        this.players = players;
        currentPlayerIndex = 0;
        board = new Board();

        boardRepresentation = new TextBoardRepresentation();

        availableTiles = new TilesNotInGame();
        for(Player player:players){
            List <Tile> temp = availableTiles.getAvailableTiles().subList(0,7);
            decks.put(player, new ArrayList<>(temp));
            for(int i=0; i<8;i++){
                availableTiles.getAvailableTiles().remove(i);
            }
        }
        wordsOnBoard = new HashMap<>();
        mainWordSquares = new ArrayList<>();
        numberOfValidMoves = 0;
        numberOfPasses = 0;
    }

    /**
     * Start the game and continue as long as players want, option to start a new game
     * @ensures the game is played until completion
     */
    public void start(){
        boolean play = true;
        while(play){
            reset();
            play();
            System.out.println("\nPlay again? (yes/no)");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            try {
                String input = bufferedReader.readLine();
                if(input.equals("yes")){
                    play = true;
                } else if(input.equals("no")){
                    play = false;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Set all attributes to the initial state
     */
    private void reset() {
        board = new Board();
        Random random = new Random();
        currentPlayerIndex = random.nextInt(players.size());
        availableTiles = new TilesNotInGame();
        for(Player player:players){
            List <Tile> temp = availableTiles.getAvailableTiles().subList(0,7);
            decks.put(player, new ArrayList<>(temp));
            player.setDeck(temp);
            for(int i=0; i<8;i++){
                availableTiles.getAvailableTiles().remove(i);
                player.setDeck(temp);
            }
        }
        wordsOnBoard = new HashMap<>();
        scores = new HashMap<>();
        for(Player player : players){
            scores.put(player,0);
        }
        mainWordSquares = new ArrayList<>();
        numberOfValidMoves = 0;
        numberOfPasses = 0;
    }

    /**
     * Play the game and printWinner() after its finished
     */
    public void play(){
        gameSituation();
        while(!gameOver()){
            boolean validTurn = makeMove();
//            if(validTurn) {
//                System.out.println("Move successful");
//            }
            if(!gameOver()){
                changeCurrentPlayerIndex();
                gameSituation();
            }
        }
        deductUnplayedLetters();
        printWinner();
    }

    /**
     * After the game has ended, deduct from each player the points for the letters not used
     * @ensures scores of players are final
     */
    private void deductUnplayedLetters() {
        if(players.size()>1){
            for(Player player : players){
                int toDeduct = 0;
                for(Tile tile : decks.get(player)){
                    toDeduct = toDeduct + tile.getPoints();
                }
                int newScore = scores.get(player) - toDeduct;
                scores.replace(player,newScore);
            }
        }
    }

    /**
     * Print to the console the winner with his/her points
     */
    public void printWinner() {
        int winnerScore = -1;
        Player winner = null;
        for(Player player : players){
            if(winnerScore<scores.get(player)){
                winnerScore = scores.get(player);
                winner = player;
            }
        }
        System.out.println();
        System.out.println("The winner is " + winner.getName() + " with "  + winnerScore + " points");
    }

    /**
     * Check if the game has finished
     * @return true if the game is over, else false
     */
    private boolean gameOver() {
        if(checkEmptyDeck() || !possibleMove() || players.size()<2){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether a player has finished all of his/her tiles
     * @return true if a player's deck is empty after a move and there are no more tiles, else false
     */
    private boolean checkEmptyDeck() {
        if(numberOfValidMoves==0){
            return false;
        }
        return decks.get(getCurrentPlayer()).isEmpty();
    }

    /**
     * Decide if there is a possible move on the board
     * @return true if it's possible to make a move on the board, else false
     */
    public boolean possibleMove(){
        if(availableTiles.getAvailableTiles().size()==0 && numberOfPasses==2){
            return false;
        } else {
            return true;
        }
    }

    /**
     * Print the current board of the game with the scores of the players and the menu for commands
     */
    public void gameSituation(){
        boardRepresentation.printBoard(board);
        System.out.println();
        for(Player player:scores.keySet()){
            System.out.println(player.getName() + " has " + scores.get(player) + " points");
        }
        printMenu();
    }

    /**
     * Print the menu for commands
     */
    private void printMenu() {
        System.out.println();
        System.out.println("General format of commands:  [COMMAND][SEPERATOR][ARGUMENT1][SEPERATOR][ARGUMENT2]... where SEPARATOR is " + ProtocolMessages.SEPARATOR);
        //System.out.println("Commands: ");
        System.out.println("To make a move - " + ProtocolMessages.MOVE + ProtocolMessages.SEPARATOR + "[NAME OF CLIENT]" + ProtocolMessages.SEPARATOR + "[COORDINATES] (Coordinates example: D1 O2 G3)");
        System.out.println("To skip a move and exchange tiles - " + ProtocolMessages.PASS + ProtocolMessages.SEPARATOR + "[TILES TO BE SWAPPED]");
        System.out.println("To leave the game - " + ProtocolMessages.ABORT + ProtocolMessages.SEPARATOR + "[NAME OF CLIENT]");
        System.out.println();
    }

    /**
     * Print the tiles of the player that has to make a move currently
     */
    public void printTiles(){
        for(Tile tile:decks.get(getCurrentPlayer())){
            System.out.print(tile.toString()+ " ");
        }
        System.out.println();
    }

    /**
     * Find and return the index of the letter,index pair
     * @requires letterIndex to be a combination of a letter or dash,letter, and an index to place the letter at
     * @param letterIndex is a String, combination of the tile to be placed, followed by the index like "D1" or "-D1"
     * @return the index of the letter,index combination
     */
    public int getIndexFromLetterIndex(String letterIndex){
        String[] letterIndexArray = letterIndex.split("");
        String indexString = "";
        if(letterIndex.charAt(0)=='-'){ //to account for blank tile
            for(int i=2; i<letterIndexArray.length;i++){
                indexString = indexString + letterIndexArray[i];
            }
        }else {
            for(int i=1; i<letterIndexArray.length;i++){
                indexString = indexString + letterIndexArray[i];
            }
        }
        return Integer.parseInt(indexString);
    }

    /**
     * Ask the player for input and if it's a valid move, do it
     * @return true if the turn has been successful, else false
     */
    public boolean makeMove(){
        Player player = getCurrentPlayer();
        System.out.println("It is " + player.getName() + "'s turn.");
        System.out.println("Your tiles: ");
        printTiles();
        String input = player.tryMove();
        String[] inputArray = input.split(ProtocolMessages.SEPARATOR);
        //invalid input handling
        String command = inputArray[0];
        switch (command){
            case ProtocolMessages.MOVE: { //M|Name|D1 O2 G3
                numberOfPasses = 0; //the player has not passed
                if(inputArray.length<3){
                    System.out.println("Invalid input");
                    return false;
                }
                String playerName = inputArray[1];
                String coordinates = inputArray[2];

                boolean blankTile = containsBlankTile(coordinates);
                String coordinatesWithoutBlank = null;
                if(blankTile){
                    coordinatesWithoutBlank = chooseLetter(coordinates);
                    if(coordinatesWithoutBlank==null){
                        System.out.println("Invalid input");
                        return false;
                    }
                }
                if(!validateMove(coordinates,blankTile,coordinatesWithoutBlank)){
                    return false;
                }
                int score = calculateScore(coordinates,blankTile,coordinatesWithoutBlank);
                fillDeck();
                player.setScore(score);
                scores.replace(player,scores.get(player) + score);
                numberOfValidMoves++;
                return true;
            }
            case ProtocolMessages.PASS: {
                numberOfPasses++;
                if(inputArray.length<2 || inputArray[1].charAt(0)==' '){
                    return false;
                }
                String tilesToExchange = inputArray[1];
                checkTilesExistInDeck(tilesToExchange);
                exchangeTiles(tilesToExchange);
                return true;
            }
            case ProtocolMessages.ABORT: {
                //String playerName = inputArray[1];
                players.remove(getCurrentPlayer());
                return true;
            }
            default: {
                System.out.println("Invalid input");
                return false;
            }
        }
    }

    /**
     * Check whether the coordinates of a move from the player contain a blank tile
     * @requires coordinates to be of type "D1 O2 -G3"
     * @param coordinates is a String to represent the move by the player
     * @return true if the coordinates contain a dash (blank tile), else false
     */
    private boolean containsBlankTile(String coordinates) {
        String[] coordinatesArray = coordinates.split(" ");
        for(String letterIndex : coordinatesArray){
            if(letterIndex.charAt(0)=='-'){
                return true;
            }
        }
        return false;
    }

    /**
     * Get the coordinates of a move and return the same coordinates without the dash
     * @requires coordinates to contain a blank tile
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     * @return coordinates but the dash representing a blank tile is removed
     */
    private String chooseLetter(String coordinates) {
        String[] coordinatesArray = coordinates.split(" ");
        String coordinatesWithoutBlank = "";
        char letter = 0;
        for(String letterIndex : coordinatesArray){
            if(letterIndex.charAt(0)=='-'){
                letter = letterIndex.charAt(1);
            }
        }
//        if(letter.length()>1){
//            return null;
//        }
        for(String letterIndex : coordinatesArray){
            if(letterIndex.charAt(0)=='-'){
                coordinatesWithoutBlank = coordinatesWithoutBlank + letter + getIndexFromLetterIndex(letterIndex) + " ";
            }else {
                coordinatesWithoutBlank = coordinatesWithoutBlank + letterIndex + " ";
            }
        }
        return coordinatesWithoutBlank;
    }

    /**
     * Change the currentPlayerIndex to that of the next player in line
     */
    private void changeCurrentPlayerIndex() {
        if(currentPlayerIndex==players.size()-1){
            currentPlayerIndex=0;
        } else {
            currentPlayerIndex++;
        }
    }

    /**
     * After placing tiles on the board, fill the deck of the player to 7 tiles
     * @requires a valid move has been made
     * @ensures players have 7 tiles in their decks as long as there are available tiles
     */
    private void fillDeck() {
        Player player = getCurrentPlayer();
        int currentNumberTiles = decks.get(player).size();
        if(availableTiles.getAvailableTiles().size()<(7-currentNumberTiles)){
            for(int i=0;i<availableTiles.getAvailableTiles().size();i++){
                Random random = new Random();
                int tileNumber = random.nextInt(availableTiles.getAvailableTiles().size());
                decks.get(player).add(availableTiles.getAvailableTiles().get(tileNumber));
                availableTiles.getAvailableTiles().remove(tileNumber);
            }
        } else {
            for(int i=0;i<7-currentNumberTiles;i++){
                Random random = new Random();
                int tileNumber = random.nextInt(availableTiles.getAvailableTiles().size());
                decks.get(player).add(availableTiles.getAvailableTiles().get(tileNumber));
                availableTiles.getAvailableTiles().remove(tileNumber);
            }
        }

    }


    /**
     * Determine whether a move made by the player is valid
     * If it's not valid, print to the console the reason why it's not
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     * @param blankTile is true when there is a blank tile in the input, else false
     * @param coordinatesWithoutBlank is a String of type "D1 O2 G3" to represent the move by the player but
     *                                without any dashes (blank tiles)
     * @return true if the move is valid, else false
     */
        public boolean validateMove(String coordinates, boolean blankTile, String coordinatesWithoutBlank){

            //Player player = getCurrentPlayer();
            if(!checkTilesExistInDeck(coordinates)){
                System.out.println("Letters not on tiles");
                return false; //letters are not corresponding to tiles
            }

            if(!validFirstTurn(coordinates)){
                System.out.println("Not a valid first turn");
                return false;
            }

            if(!connectedToWordsOnBoard(coordinates)){
                System.out.println("Not connected to words on the board");
                return false;
            }

            String[] coordinatesArray=null;
            if(blankTile){
                coordinatesArray = coordinatesWithoutBlank.split(" ");
            }else {
                coordinatesArray = coordinates.split(" "); //1 - D1, 2 - O2, 3 - G3
            }
            //words can be vertical ot horizontal - two diff approaches
            boolean horizontal;
            int firstLetterIndex = getIndexFromLetterIndex(coordinatesArray[0]);
            //find whether the word is horizontal or vertical
            if(coordinatesArray.length==1){
                if(letterBefore(firstLetterIndex) || letterAfter(firstLetterIndex)){
                    horizontal=true;
                } else {
                    horizontal=false;
                }
            } else {
                int secondLetterIndex = getIndexFromLetterIndex(coordinatesArray[1]);
                if((secondLetterIndex-firstLetterIndex)%15==0){
                    horizontal = false;
                } else {
                    horizontal = true;
            }
        }
            this.horizontal = horizontal;
            //System.out.println(horizontal + "- horizontal");
            int lastLetterIndex = getIndexFromLetterIndex(coordinatesArray[coordinatesArray.length-1]);

            if(horizontal){ // horizontal
                String wordBefore = "";
                String wordAfter = "";
                String mainWord = "";
                if(letterBefore(firstLetterIndex)){
                    wordBefore = wordBefore(firstLetterIndex);
                }
                if(letterAfter(lastLetterIndex)){
                    wordAfter = wordAfter(lastLetterIndex);
                }

                Board boardTemp = board.copy();
                placeTilesOnBoard(boardTemp,coordinates);
                if(blankTile){
                    mainWord = mainWordHorizontal(boardTemp,coordinatesWithoutBlank); //get word between firstLetterIndex and lastLetterIndex
                }else {
                    mainWord = mainWordHorizontal(boardTemp,coordinates);
                }


                String word = wordBefore + mainWord + wordAfter;
                //System.out.println(wordBefore + " - wordBefore  " + mainWord + "- mainWord  " + wordAfter + "-wordAfter  " + word);//word combined from the letters before, those from first to last index and the letters after
                if(!isValidWord(word)){
                    System.out.println("Invalid horizontal main word");
                    return false;
                }else {
                    List<String> adjacentWords;

                    if(blankTile){
                        adjacentWords = adjacentHorizontal(coordinatesWithoutBlank);
                    }else {
                        adjacentWords = adjacentHorizontal(coordinates);
                    }

                    //System.out.println(adjacentWords);

                    if(!adjacentWords.isEmpty()){
                        for(String adjacentWord:adjacentWords){
                            if(!isValidWord(adjacentWord)){
                                System.out.println("Invalid adjacent words");
                                return false;
                            }
                        }
                    }
                    //place tiles on board, remove tiles from deck
                    placeTilesOnBoard(board,coordinates);
                    removeTilesFromDeck(coordinates);
                    return true;
                }
            } else { //vertical
                String wordAbove = "";
                String wordBelow = "";
                String mainWord = "";
                if(letterAbove(firstLetterIndex)){
                    wordAbove = wordAbove(firstLetterIndex);
                }
                if (letterBelow(lastLetterIndex)) {
                    wordBelow = wordBelow(lastLetterIndex);
                }

                Board boardTemp = board.copy();
                placeTilesOnBoard(boardTemp,coordinates); // place tiles on cloned board
                if(blankTile){
                    mainWord = mainWordVertical(boardTemp,coordinatesWithoutBlank); //get word between firstLetterIndex and lastLetterIndex
                }else {
                    mainWord = mainWordVertical(boardTemp,coordinates);
                }


                String word = wordAbove + mainWord + wordBelow; //word combined from the letters above, those from first to last index and the letters below
                if(!isValidWord(word)){
                    System.out.println("Invalid main word");
                    return false;
                }else {
                    List <String> adjacentWords = null;
                    if(blankTile){
                        adjacentWords = adjacentVertical(coordinatesWithoutBlank);
                    }else {
                        adjacentWords = adjacentVertical(coordinates);
                    }

                    //System.out.println(adjacentWords);
                    if(!adjacentWords.isEmpty()){
                        for(String adjacentWord:adjacentWords){
                            if(!isValidWord(adjacentWord)){
                                System.out.println("Invalid adjacent words");
                                return false;
                            }
                        }
                    }
                    placeTilesOnBoard(board,coordinates);
                    removeTilesFromDeck(coordinates);
                    return true;
                }
            }
    }

    /**
     * Check whether the word to be placed is valid for the indexes it should take
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     * @return true if the coordinates are connected to words on board, else false
     */
    private boolean connectedToWordsOnBoard(String coordinates) {
        if(numberOfValidMoves!=0){
            String[] coordinatesArray = coordinates.split(" ");
            boolean connected = false;
            for(String letterIndex : coordinatesArray){
                int index = getIndexFromLetterIndex(letterIndex);
                if(letterBefore(index) || letterAfter(index) || letterBelow(index) || letterAbove(index)){
                    connected = true;
                }
            }
            return connected;
        }else {
            return true;
        }

    }

    /**
     * Check whether the move if a first turn, is valid
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     * @return true if the move uses the center square or if it's not a first move, else false
     */
    private boolean validFirstTurn(String coordinates) {
        String[] coordinatesArray = coordinates.split(" ");
        if(numberOfValidMoves==0){
            boolean check = false;
            for(String letterIndex : coordinatesArray){
                int index = getIndexFromLetterIndex(letterIndex);
                //System.out.println(letterIndex + "  " +index);
                if(index==112){
                    check = true;
                }
            }
            if(check){
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * Check whether there is a letter to the left of the Square with the given index
     * @param index of the Square to check
     * @return true if there is a letter placed on the board before (to the left) of the given index, else false
     */
    public boolean letterBefore(int index){
        for(int i=0;i<211;i=i+15){
            if(index==i){
                return false;
            }
        }
        if(board.getSquare(index-1).getTile()!=null){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether there is a letter to the right of the Square with the given index
     * @param index of the Square to check
     * @return true if there is a letter placed on the board after (to the right) of the given index, else false
     */
    public boolean letterAfter(int index){
        for(int i=14;i<225;i=i+15){
            if(index==i){
                return false;
            }
        }
        if(board.getSquare(index+1).getTile()!=null){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether there is a letter above the Square with the given index
     * @param index of the Square to check
     * @return true if there is a letter placed on the board above the given index, else false
     */
    public boolean letterAbove(int index){
        if(index>=0 && index<=14){
            return false;
        }
        if(board.getSquare(index-15).getTile()!=null){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether there is a letter below the Square with the given index
     * @param index of the Square to check
     * @return true if there is a letter placed on the board below the given index, else false
     */
    public boolean letterBelow(int index){
        if(index>=210 && index<=224){
            return false;
        }
        if(board.getSquare(index+15).getTile()!=null){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Find the letters to the left of the Square with the given index
     * @param index of the Square to get the letters before in the same row
     * @return a String of the letters before (to the left) the Square of the given index
     */
    public String wordBefore(int index) {
        String result = "";
        for(int i = index-1;i>=15*Square.getRowFromIndex(index);i--){
            if(board.getSquare(i).getTile()!=null){
                char letter = board.getSquare(i).getTile().getLetter();
                result = letter + result;
            } else {
                break;
            }
        }
        return result;
    }

    /**
     * Find the letters to the right of the Square with the given index
     * @param index of the Square to get the letters after in the same row
     * @return a String of the letters after (to the right) the Square of the given index
     */
    public String wordAfter(int index){
        String result = "";
        for(int i = index+1;i<=14+15*Square.getRowFromIndex(index);i++){
            if(board.getSquare(i).getTile()!=null){
                char letter = board.getSquare(i).getTile().getLetter();
                result = result + letter;
            } else {
                break;
            }
        }
        return result;
    }

    /**
     * Find the letters above the Square with the given index
     * @param index of the Square to get the letters above in the same column
     * @return a String of the letters above the Square of the given index
     */
    private String wordAbove(int index) {
        String result = "";
        for(int i = index-15; i>=Square.getColFromIndex(index);i=i-15){
            if(board.getSquare(i).getTile()!=null){
                char letter = board.getSquare(i).getTile().getLetter();
                result = letter + result;
            } else {
                break;
            }
        }
        return result;
    }

    /**
     * Find the letters below the Square with the given index
     * @param index of the Square to get the letters below in the same column
     * @return a String of the letters below the Square of the given index
     */
    private String wordBelow(int index) {
        String result = "";
        for(int i = index+15;i<=210 + Square.getColFromIndex(index);i++){
            if(board.getSquare(i).getTile()!=null){
                char letter = board.getSquare(i).getTile().getLetter();
                result = result + letter;
            } else {
                break;
            }
        }
        return result;
    }

    /**
     * Finds the main word that the played tiles would produce from first to last letter of input,
     * considering the words already on the board
     * @param boardTemp a temporary board where the tiles of the move are placed before validating
     * @param coordinates is a String of type "D1 O2 G3" (no dashes) to represent the move by the player
     * @return the main word produced from a horizontal move
     */
    private String mainWordHorizontal(Board boardTemp, String coordinates) {
        String result = "";
        String[] coordinatesArray = coordinates.split(" ");
        int firstLetterIndex = getIndexFromLetterIndex(coordinatesArray[0]);
        int lastLetterIndex = getIndexFromLetterIndex(coordinatesArray[coordinatesArray.length-1]);
        for(int i = firstLetterIndex;i<=lastLetterIndex;i++){
            if(boardTemp.getSquare(i).getTile()==null){
                System.out.println(i + " - index");
                System.out.println("Wrong user input");
            }
            result = result + boardTemp.getSquare(i).getTile().getLetter();
        }
        return result;
    }

    /**
     * Finds the main word that the played tiles would produce from first to last letter of input,
     * considering the words already on the board
     * @param boardTemp a temporary board where the tiles of the move are placed before validating
     * @param coordinates is a String of type "D1 O2 G3" (no dashes) to represent the move by the player
     * @return the main word produced from a vertical move
     */
    private String mainWordVertical(Board boardTemp, String coordinates) {
        String result = "";
        String[] coordinatesArray = coordinates.split(" ");
        int firstLetterIndex = getIndexFromLetterIndex(coordinatesArray[0]);
        int lastLetterIndex = getIndexFromLetterIndex(coordinatesArray[coordinatesArray.length-1]);
        for(int i = firstLetterIndex;i<=lastLetterIndex;i=i+15){
            if(boardTemp.getSquare(i).getTile()==null){
                System.out.println(i + " - index");
                System.out.println("Wrong user input"); //dunno when its checked/how to check
            }
            //mainWordSquares.add(boardTemp.getSquare(i)); //list of squares of the main word
            result = result + boardTemp.getSquare(i).getTile().getLetter(); //not working sometimes, dunno whats wrong
        }
        return result;
    }

    /**
     * Find the adjacent words to the main word (horizontal) from input
     * @param coordinates is a String of type "D1 O2 G3" (no dashes) to represent the move by the player
     * @return a List of the adjacent words to the word played
     */
    public List<String> adjacentHorizontal(String coordinates){
        String[] coordinatesArray = coordinates.split(" ");
        List<String> adjacentWords = new ArrayList<>();

        for(String letterIndex:coordinatesArray){
            String wordAbove = "";
            String wordBelow = "";
            char letter = letterIndex.charAt(0);
            int index = getIndexFromLetterIndex(letterIndex);
            if(letterAbove(index)){
                wordAbove = wordAbove(index);
            }
            if(letterBelow(index)){
                wordBelow = wordBelow(index);
            }

            if(letterAbove(index) || letterBelow(index)){
                String word = wordAbove + letter + wordBelow;
                adjacentWords.add(word);
            }

        }
        return adjacentWords;
    }

    /**
     * Find the adjacent words to the main word (vertical) from input
     * @param coordinates is a String of type "D1 O2 G3" (no dashes) to represent the move by the player
     * @return a List of the adjacent words to the word played
     */
    public List <String> adjacentVertical(String coordinates){
        String[] coordinatesArray = coordinates.split(" ");
        List<String> adjacentWords = new ArrayList<>();

        for(String letterIndex:coordinatesArray){
            String wordBefore = "";
            String wordAfter = "";
            char letter = letterIndex.charAt(0);
            int index = getIndexFromLetterIndex(letterIndex);
            if(letterBefore(index)){
                wordBefore = wordBefore(index);
            }
            if(letterAfter(index)){
                wordAfter = wordAfter(index);
            }

            if(letterBefore(index) || letterAfter(index)){
                String word = wordBefore + letter + wordAfter;
                adjacentWords.add(word);
            }

        }
        return adjacentWords;
    }

    /**
     * Place the tiles of the move on a board
     * @param boardTemp a board to place the given tiles on
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     */
    private void placeTilesOnBoard(Board boardTemp, String coordinates) { //place tiles on some board
        String[] coordinatesArray = coordinates.split(" ");
        Player player = getCurrentPlayer();

        for(String letterIndex : coordinatesArray){
            char letter = letterIndex.charAt(0);
            int index = getIndexFromLetterIndex(letterIndex);
            Tile tileToAdd = null;
            if(letter=='-'){
                letter = letterIndex.charAt(1);
                tileToAdd = getTileFromAll(letter);
            } else {
                for(Tile tile : decks.get(player)){
                    if(tile.getLetter()==letter){
                        tileToAdd=tile;
                    }
                }
            }
            boardTemp.placeTile(index,tileToAdd);
        }
    }

    /**
     * Find a Tile that corresponds to the given letter
     * @param letter to get the according tile for
     * @return a Tile of the letter
     */
    private Tile getTileFromAll(char letter){
        Map<Tile,Integer> allTiles = Board.allTiles();
        for(Tile tile : allTiles.keySet()){
            if(tile.getLetter()==letter){
                return tile;
            }
        }
        System.out.println("no letter provided to getTile");
        return null;
    }

    /**
     * Remove the tiles played from the corresponding player's deck
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     */
    private void removeTilesFromDeck(String coordinates) {
        Player player = getCurrentPlayer();
        String[] coordinatesArray = coordinates.split(" ");
        for(String letterIndex:coordinatesArray){
            decks.get(player).removeIf(tile -> tile.getLetter() == letterIndex.charAt(0));
        }
    }

    /**
     * Check whether the letters in coordinates or tiles to exchange, correspond to tiles in the current player's deck
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     * @return true if the letters are available as tiles in the deck on the current player, else false
     */
    public boolean checkTilesExistInDeck(String coordinates){
        Player player = getCurrentPlayer();
        String[] coordinatesArray = coordinates.split(" ");
        for(String letterSquare:coordinatesArray){
            boolean exist=false;
            for(Tile tile : decks.get(player)){
                if(tile.getLetter()==letterSquare.charAt(0)){
                    exist = true;
                }
            }
            if(!exist){
                return false;
            }
        }
        return true;
    }

    /**
     * Check whether the given word is a valid word according to the allowed scrabble words
     * @param word to be checked
     * @return true if the word is valid, else false
     */
    public boolean isValidWord(String word){
        if(scrabbleWordChecker.isValidWord(word)!=null){
            return true;
        } else {
            return false;
        }
    }

    //gets a String[] of the letters of the words, e.g. D,O,G
    public int calculateWord(String[] word){
        int score = 0;
        for(String letter:word){
            for(Tile tile:Board.allTiles().keySet()){
                if(tile.getLetter()==letter.charAt(0)){
                    score+= tile.getPoints();
                    break;
                }
            }
        }
        return score;
    }

    /**
     * Calculate the score that a word/combination of letters would produce without multipliers
     * @param word or letters to be calculated the score
     * @return the score for the given combination of letters
     */
    public int calculateWord(String word){ //word w + o + r + d, w!rd w + ! + r + d
        int score = 0;
        if(word.equals("")){
            return 0;
        }
        String[] wordArray = word.split("");
        for(String letter:wordArray){
            for(Tile tile:Board.allTiles().keySet()){
                if(tile.getLetter()==letter.charAt(0)){
                    score+= tile.getPoints();
                    break;
                }
            }
        }
        return score;
    }

    /**
     * Calculate the score from the main word, combined with the scores of the letters before, after or above, below if any
     * @param firstLetterIndex is the index of the first letter placed on the board this turn
     * @param lastLetterIndex is the index of the last letter placed on the board this turn
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     * @param blankTile true if there is a blank tile in coordinates, else false
     * @param coordinatesWithoutBlank is a String of type "D1 O2 G3" (no dashes) to represent the move by the player
     * @return the score for the main word
     */
    public int calculateCombinedMainWord(int firstLetterIndex, int lastLetterIndex,String coordinates,boolean blankTile, String coordinatesWithoutBlank){
        int score = 0;
        if (blankTile) {
            if(horizontal){
                score = calculateWord(wordBefore(firstLetterIndex)) + calculateWord(wordAfter(lastLetterIndex)) + calculateHorizontalMainWordWithoutInput(firstLetterIndex, lastLetterIndex, coordinatesWithoutBlank);
                //System.out.println(score + " - score of wordBefore: "+calculateWord(wordBefore(firstLetterIndex))+", wordAfter: "+calculateWord(wordAfter(lastLetterIndex))+", horizontalWithoutInput");
            } else {
                score = calculateWord(wordAbove(firstLetterIndex)) + calculateWord(wordBelow(lastLetterIndex)) + calculateVerticalMainWordWithoutInput(firstLetterIndex, lastLetterIndex, coordinatesWithoutBlank);
            }
        }else {
            if(horizontal){
                score = calculateWord(wordBefore(firstLetterIndex)) + calculateWord(wordAfter(lastLetterIndex)) + calculateHorizontalMainWordWithoutInput(firstLetterIndex, lastLetterIndex, coordinates);
                //System.out.println(score + " - score of wordBefore: "+calculateWord(wordBefore(firstLetterIndex))+", wordAfter: "+calculateWord(wordAfter(lastLetterIndex))+", horizontalWithoutInput");
            } else {
                score = calculateWord(wordAbove(firstLetterIndex)) + calculateWord(wordBelow(lastLetterIndex)) + calculateVerticalMainWordWithoutInput(firstLetterIndex, lastLetterIndex, coordinates);
            }
        }

        boolean tripleWord = false;
        int tripleWordCounter = 0;
        boolean doubleWord = false;
        int doubleWordCounter = 0;
        char letterForBlank = 0;
        int indexBlank = 0;
        if(blankTile){
            String[] coordinatesArray = coordinates.split(" ");
            for(String letterIndex : coordinatesArray){
                if(letterIndex.charAt(0)=='-'){
                    letterForBlank = letterIndex.charAt(1);
                    indexBlank = getIndexFromLetterIndex(letterIndex);
                }
            }
        }
        for(Square square : mainWordSquares){
            //char letter = square.getTile().getLetter();
            int letterScore = 0;
            letterScore = square.getTile().getPoints();
            if(square.getTile().getLetter()==letterForBlank && square.getIndex()==indexBlank){
                letterScore=0;
            }

            if(square.getType() == Square.Type.TRIPLE_WORD){
                tripleWord = true;
                tripleWordCounter++;
            } else if(square.getType() == Square.Type.DOUBLE_WORD || square.getType() == Square.Type.CENTER){
                doubleWord = true;
                doubleWordCounter++;
            } else if(square.getType() == Square.Type.DOUBLE_LETTER ){
                letterScore = 2 * letterScore;
            } else if(square.getType() == Square.Type.TRIPLE_LETTER){
                letterScore = 3 * letterScore;
            }
            //System.out.println(letterScore + " - letterScore for " + square.getTile().getLetter());
            score = score + letterScore;
        }
        if(tripleWord){
            score = (int) (score * Math.pow(3,tripleWordCounter));
        }
        if(doubleWord){
            score = (int) (score * Math.pow(2,doubleWordCounter));
        }
        return score;
    }

    /**
     * Calculate the points of letters of the main word that are between those from input, horizontal
     * @param firstLetterIndex is the index of the first letter placed on the board this turn
     * @param lastLetterIndex is the index of the last letter placed on the board this turn
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     * @return the score of the letters in the main word that are already placed on the board
     */
    private int calculateHorizontalMainWordWithoutInput(int firstLetterIndex, int lastLetterIndex, String coordinates) {
        String[] coordinatesArray = coordinates.split(" ");
        int score = 0;
        for(int i = firstLetterIndex; i<=lastLetterIndex;i++){
            boolean check = false;
            for(String letterIndex : coordinatesArray){
                if(board.getSquare(i).getTile().getLetter()==letterIndex.charAt(0) &&
                i==getIndexFromLetterIndex(letterIndex)){
                    check = true;
                }
            }
            if(!check){
                score = score + board.getSquare(i).getTile().getPoints();
            }
        }
        return score;
    }

    /**
     * Calculate the points of letters of the main word that are between those from input, vertical
     * @param firstLetterIndex is the index of the first letter placed on the board this turn
     * @param lastLetterIndex is the index of the last letter placed on the board this turn
     * @param coordinates is a String of type "D1 O2 G3" (no dashes) to represent the move by the player
     * @return the score of the letters in the main word that are already placed on the board
     */
    private int calculateVerticalMainWordWithoutInput(int firstLetterIndex, int lastLetterIndex, String coordinates){
        String[] coordinatesArray = coordinates.split(" ");
        int score = 0;
        for(int i = firstLetterIndex; i<lastLetterIndex;i=i+15){
            boolean check = false;
            for(String letterIndex : coordinatesArray){
                if(board.getSquare(i).getTile().getLetter()==letterIndex.charAt(0) &&
                        i==getIndexFromLetterIndex(letterIndex)){
                    check = true;
                }
            }
            if(!check){
                score = score + board.getSquare(i).getTile().getPoints();
            }
        }
        return score;
    }

    /**
     * Set mainWordSquares to a List of the current squares that the tiles from input occupy
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     */
    private void setMainWordSquares(String coordinates){
        this.mainWordSquares = new ArrayList<>();
        String[] coordinatesArray = coordinates.split(" ");
        for(String letterIndex : coordinatesArray){
            int index = getIndexFromLetterIndex(letterIndex);
            mainWordSquares.add(board.getSquare(index));
        }
    }

    /**
     * Calculate the score of the adjacent words to letters from input
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     * @return the points from the adjacent words to letters from input
     */
    private int calculateAdjacentWords(String coordinates) {
        int score = 0;
        String[] coordinatesArray = coordinates.split(" ");
        for(String letterIndex : coordinatesArray){
            int scoreTemp = 0;
            boolean tripleWord = false;
            boolean doubleWord = false;
            //char letter = letterIndex.charAt(0);
            int index = getIndexFromLetterIndex(letterIndex);
            int letterScore = board.getSquare(index).getTile().getPoints();
            if(horizontal){
                if(letterAbove(index)){
                    scoreTemp = scoreTemp + calculateWord(wordAbove(index));
                }
                if(letterBelow(index)){
                    scoreTemp = scoreTemp + calculateWord(wordBelow(index));
                }
            } else {
                if(letterBefore(index)){
                    scoreTemp = scoreTemp + calculateWord(wordBefore(index));
                }
                if(letterAfter(index)){
                    scoreTemp = scoreTemp + calculateWord(wordAfter(index));
                }
            }
            if(board.getSquare(index).getType() == Square.Type.TRIPLE_WORD){
                tripleWord = true;
            } else if(board.getSquare(index).getType() == Square.Type.DOUBLE_WORD
                    || board.getSquare(index).getType() == Square.Type.CENTER){
                doubleWord = true;
            } else if(board.getSquare(index).getType() == Square.Type.DOUBLE_LETTER){
                letterScore = 2 * letterScore;
            } else if(board.getSquare(index).getType() == Square.Type.TRIPLE_LETTER){
                letterScore = 3 * letterScore;
            }

            if(scoreTemp>0){
                scoreTemp = scoreTemp + letterScore;
            }

            if(tripleWord){
                scoreTemp = scoreTemp * 3;
            }
            if(doubleWord){
                scoreTemp = scoreTemp * 2;
            }

            score = score + scoreTemp;
        }
        return score;
    }

    /**
     * Calculate the full score a player should receive for a round
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     * @param blankTile true if there is a blank tile in coordinates, else false
     * @param coordinatesWithoutBlank is a String of type "D1 O2 G3" (no dashes) to represent the move by the player
     * @return the full score the player should receive for the turn
     */
    public int calculateScore(String coordinates,boolean blankTile, String coordinatesWithoutBlank){
        int score = 0;
        setMainWordSquares(coordinates);
        String[] coordinatesArray = coordinates.split(" ");
        int firstLetterIndex = getIndexFromLetterIndex(coordinatesArray[0]);
        int lastLetterIndex = getIndexFromLetterIndex(coordinatesArray[coordinatesArray.length-1]);
        score = score + calculateCombinedMainWord(firstLetterIndex,lastLetterIndex,coordinates,blankTile,coordinatesWithoutBlank);
        //System.out.println(score + " - main word");
        score = score + calculateAdjacentWords(coordinates);
        //System.out.println(score + " - main word + adj words");
        if(checkBingo(coordinates)){
            score = score + BINGO;
        }
        return score;
    }

    /**
     * Check whether the move is a BINGO if valid
     * @param coordinates is a String of type "D1 O2 -G3" to represent the move by the player
     * @return true if there is a BINGO, else false
     */
    private boolean checkBingo(String coordinates) {
        String[] coordinatesArray = coordinates.split(" ");
        if(coordinatesArray.length==7){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove from the player's deck the tiles from tilesToExchange and add as many from the available ones
     * @param tilesToExchange is a String with letters to represent the tiles and spaces between them
     */
    private void exchangeTiles(String tilesToExchange) {
        String[] tilesToExchangeArray = tilesToExchange.split(" ");
        List<Tile> tilesToRemove = new ArrayList<>();
        for(String tileString : tilesToExchangeArray){
            for(Tile tile : decks.get(getCurrentPlayer())){
                if(tileString.charAt(0)==tile.getLetter()){
                    tilesToRemove.add(tile);
                    break;
                    }
                }
        }
        for(Tile tile : tilesToRemove){
            decks.get(getCurrentPlayer()).remove(tile);
            decks.get(getCurrentPlayer()).add(availableTiles.getAvailableTiles().get(0));
            availableTiles.getAvailableTiles().remove(0);
            availableTiles.getAvailableTiles().add(tile);
        }
    }

    /**
     * Find the player whose turn it is
     * @return the player whose turn it is
     */
    public Player getCurrentPlayer() {
        return players.get(currentPlayerIndex);
    }

}
