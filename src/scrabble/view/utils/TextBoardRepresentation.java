package scrabble.view.utils;

import scrabble.model.Board;
import scrabble.model.Square;
import scrabble.model.Tile;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TextBoardRepresentation {
    public static final int SIZE = 15;

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    private static final String[] NUMBERING = {"1 2 3 4 5 6 7 8 9 10 11 12 13 14 15", "  |  A  |  B  |  C  |  D  |  E  |  F  |  G  |  H  |  I  |  J  |  K  |  L  |  M  |  N  |  O  |  "};

    public void printBoard(Board board){
        System.out.println(ANSI_WHITE_BACKGROUND + NUMBERING[1] + ANSI_RESET);
        for(int row = 0; row<SIZE; row++){
            for(int col=0;col<SIZE;col++){
                Square temp = board.getSquare(row,col);
                String toPrint = "";
                if(temp.getTile()==null){
                    toPrint = "     ";
                } else {
                    toPrint = "  "+temp.getTile().getLetter()+"  " + ANSI_RESET;
                }
                if(col==0){
                    String rowToPrint = " " + (row + 1);
                    if(row>8) {
                        rowToPrint = String.valueOf(row + 1);
                    }
                    System.out.print(ANSI_WHITE_BACKGROUND + rowToPrint +   "|" + ANSI_RESET + decideColor(temp) + toPrint + ANSI_RESET);
                } else {
                    System.out.print("|" + decideColor(temp) + toPrint + "");
                    System.out.print(ANSI_RESET + "");
                }
            }
            System.out.println(ANSI_WHITE_BACKGROUND + "|  "+ ANSI_RESET);
            if(row==SIZE-1){
                System.out.println(ANSI_WHITE_BACKGROUND  + "                                                                                               " + ANSI_RESET);
            }else {
                System.out.println(ANSI_WHITE_BACKGROUND + "   "+ ANSI_RESET +"                                                                                         "
                        +ANSI_WHITE_BACKGROUND+ "   " +ANSI_RESET);

            }
        }
    }

    public String printBoardOnline(Board board){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        PrintStream old = System.out;
        System.setOut(ps);
        System.out.println(ANSI_WHITE_BACKGROUND + NUMBERING[1] + ANSI_RESET);
        for(int row = 0; row<SIZE; row++){
            for(int col=0;col<SIZE;col++){
                Square temp = board.getSquare(row,col);
                String toPrint = "";
                if(temp.getTile()==null){
                    toPrint = "     ";
                } else {
                    toPrint = "  "+temp.getTile().getLetter()+"  " + ANSI_RESET;
                }
                if(col==0){
                    String rowToPrint = " " + (row + 1);
                    if(row>8) {
                        rowToPrint = String.valueOf(row + 1);
                    }
                    System.out.print(ANSI_WHITE_BACKGROUND + rowToPrint +   "|" + ANSI_RESET + decideColor(temp) + toPrint + ANSI_RESET);
                } else {
                    System.out.print("|" + decideColor(temp) + toPrint + "");
                    System.out.print(ANSI_RESET + "");
                }
            }
            System.out.println(ANSI_WHITE_BACKGROUND + "|  "+ ANSI_RESET);
            if(row==SIZE-1){
                System.out.println(ANSI_WHITE_BACKGROUND  + "                                                                                               " + ANSI_RESET);
            }else {
                System.out.println(ANSI_WHITE_BACKGROUND + "   "+ ANSI_RESET +"                                                                                         "
                        +ANSI_WHITE_BACKGROUND+ "   " +ANSI_RESET);

            }
        }
        //System.out.println();
        System.out.flush();
        System.setOut(old);
        return baos.toString();
    }

    private String decideColor(Square square){
        switch(square.getType()){
            case CENTER:
            case DOUBLE_WORD: {
                return ANSI_PURPLE_BACKGROUND;
                //break;
            }
            case NORMAL: {
                return ANSI_RESET;
                //break;
            }
            case DOUBLE_LETTER: {
                return ANSI_CYAN_BACKGROUND;
                //break;
            }
            case TRIPLE_LETTER: {
                return ANSI_BLUE_BACKGROUND;
                //break;
            }
            case TRIPLE_WORD: {
                return ANSI_RED_BACKGROUND;
                //break;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        Board board = new Board();
        TextBoardRepresentation boardRepresentation = new TextBoardRepresentation();
        //boardRepresentation.printBoard(board);
        //System.out.println("---------------------------------------------------");
        System.out.println(boardRepresentation.printBoardOnline(board));
    }
}
