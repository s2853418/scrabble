package scrabble.services;

import scrabble.model.Board;
import scrabble.model.Tile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class TilesNotInGame {

    private List<Tile> availableTiles; //list of tiles not on board or in players

    public List<Tile> getAvailableTiles() {
        return availableTiles;
    }

    public TilesNotInGame() {
        availableTiles = new ArrayList<>();
        generateTiles();
        shuffle();
    }

    public void generateTiles(){
        Map<Tile,Integer> temp = Board.allTiles();
        for(Tile tile:temp.keySet()){
            for (int i=0;i<temp.get(tile);i++){
                availableTiles.add(tile);
            }
        }
    }

    public void shuffle() {
        Collections.shuffle(availableTiles);
    }
}
