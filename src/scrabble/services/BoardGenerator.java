package scrabble.services;

import scrabble.model.Square;

public class BoardGenerator {

        private static final int SIZE = 15;
        private Square[][] squares;

        public BoardGenerator() {
                squares = new Square[SIZE][SIZE];
        }

        public void generateSquares(){
                for(int row=0;row<SIZE;row++){
                        for(int col=0;col<SIZE;col++){
                                if(col==SIZE/2 && row==SIZE/2){
                                        squares[row][col] = new Square(row,col, Square.Type.CENTER);
                                } else if ((row==SIZE-SIZE && (col==SIZE-SIZE || col==SIZE/2 || col==SIZE-1)) ||
                                           (row==SIZE/2 && (col==SIZE-SIZE || col==SIZE-1)) ||
                                           (row==SIZE-1 && (col==SIZE-SIZE || col==SIZE/2 || col==SIZE-1)) ){
                                        squares[row][col] = new Square(row,col, Square.Type.TRIPLE_WORD);
                                } else if( ((row==SIZE-SIZE || row==SIZE-1) && (col==SIZE/4 || col==SIZE-SIZE/4-1)) || //1 and 15
                                           ((row==SIZE/4-1 || row==SIZE-SIZE/4) && (col==SIZE/2-1 || col==SIZE/2+1)) || //3 and 13
                                           ((row==SIZE/4 || row==SIZE-SIZE/4-1) && (col==SIZE-SIZE || col==SIZE/2 || col==SIZE-1)) || //4 and 12
                                           ((row==SIZE/2-1 || row==SIZE/2+1) && (col==SIZE/4-1 || col==SIZE/2-1 || col==SIZE/2+1 || col==SIZE-SIZE/4)) || //7 & 9
                                           (row==SIZE/2 && (col==SIZE/4 || col==SIZE-SIZE/4-1)) ){ //8
                                        squares[row][col] = new Square(row,col, Square.Type.DOUBLE_LETTER);
                                } else if( ((row==SIZE-SIZE+1 || row==SIZE-2) && (col==SIZE-SIZE+1 || col==SIZE-2)) || //2 & 14
                                           ((row==SIZE/4-1 || row==SIZE-SIZE/4) && (col==SIZE/4-1 || col==SIZE-SIZE/4)) || //3 & 13
                                           ((row==SIZE/4 || row==SIZE-SIZE/4-1) && (col==SIZE/4 || col==SIZE-SIZE/4-1)) || //4 & 12
                                           ((row==SIZE/4+1 || row==SIZE/2+SIZE/4) && (col==SIZE/4+1 || col==SIZE/2+SIZE/4))  ){ //5 & 11
                                        squares[row][col] = new Square(row,col, Square.Type.DOUBLE_WORD);
                                } else if( ((row==SIZE-SIZE+1 || row==SIZE-2) && (col==SIZE/2-SIZE/4+1 || col==SIZE/2+SIZE/4-1)) || //2 & 14
                                           ((row==SIZE/2-SIZE/4+1 || row==SIZE/2+SIZE/4-1) && (col==1 || col==SIZE/2-SIZE/4+1 || col==SIZE/2+SIZE/4-1 || col==SIZE-2)) ){ //6 & 10
                                        squares[row][col] = new Square(row,col, Square.Type.TRIPLE_LETTER);
                                } else {
                                        squares[row][col] = new Square(row,col, Square.Type.NORMAL);
                                }
                        }
                }
        }

        public Square[][] getEmptyBoard(){
                generateSquares();
                return squares;
        }
}
